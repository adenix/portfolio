import React from 'react'
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import 'normalize.css';
import './index.sass';

import './global/scripts/fontawesome/fontawesome.min';
import './global/scripts/fontawesome/brands.min';
import './global/scripts/modernizr/modernizr-custom.js';

import Home from "./views/home";

setTimeout(() => {

  render(
    <Router>
      <Switch>
        <Route path="/" component={Home}/>
      </Switch>
    </Router>,
    document.getElementById('root')
  );

}, 1000);

import ConsoleGems from './global/scripts/console.gem';

ConsoleGems();
