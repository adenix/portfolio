import React from 'react';

import './cursor.component.sass';

const Cursor = ({ blink }) => (
  <span className="cursor" blink={blink}>|</span>
);

export default Cursor;
