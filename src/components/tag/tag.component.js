import React from 'react';

import './tag.component.sass';

const Tag = ({ href, children }) => (
  <a className="tag" href={href}>{children}</a>
);

export default Tag;
