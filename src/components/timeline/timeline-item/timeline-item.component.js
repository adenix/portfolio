import React from 'react';

import './timeline-item.component.sass';

const TimelineItem = ({ date, sub, title, children }) => (
  <li className="timeline-item">
    <div className="timeline-left">
      <div className="timeline-date">{date}</div>
      <div className="timeline-sub">{sub}</div>
    </div>
    <div className="timeline-content">
      <div className="timeline-title">{title}</div>
      {children}
    </div>
  </li>
);

export default TimelineItem;
