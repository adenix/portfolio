import React from 'react';

import './timeline.component.sass';

const Timeline = ({ children }) => (
  <ol className="timeline">
    {children}
  </ol>
);

export default Timeline;
