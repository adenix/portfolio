import React from 'react';

import "./command.component.sass";

const Command = ({ statement, responses }) => (
  <div className="console-command">
    <div className="console-statement">
      {statement}
    </div>
    <div className="console-responses">
      {
        responseSyntax(responses)
      }
    </div>
  </div>
);

const responseSyntax = (responses) => {
  if(responses && responses.length > 1) {
    return (
      <ul>
        {
          responses.map((response, i) => {
            if(response.href)
              return (
                <li key={i}><a href={response.href} target="_blank">{response.body}</a></li>
              );
            else
              return (
                <li key={i}>{response.body}</li>
              )
          })
        }
      </ul>
    )
  } else {
    let response = responses[0];

    if(response.href)
      return (
        <a href={response.href} target="_blank">{response.body}</a>
      );
    else
      return (
        <span>{response.body}</span>
      )
  }
};

export default Command;
