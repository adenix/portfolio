import React from 'react';

import "./console.component.sass";

const Console = ({ children }) => (
  <div className="console" shadow="false">
    {children}
  </div>
);

export default Console;
