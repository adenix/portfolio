import React from 'react';

import './grid.component.sass';

const Grid = ({ children }) => (
  <div className="grid">
    {children}
  </div>
);

export default Grid;
