import React from 'react';

import './browser.component.sass';

const Browser = ({ protocol, baseUrl, path, img }) => (
  <div className="browse browse-shadow">
    <div className="browse-bar">
      <ul className="browse-ctrs">
        <li className="browse-ctr browse-close"></li>
        <li className="browse-ctr browse-min"></li>
        <li className="browse-ctr browse-full"></li>
      </ul>
      <div className="browse-address"><span className="browse-protocol">{protocol}</span>{baseUrl}<span className="browse-path">{path}</span></div>
    </div>
    <div className="browse-screen">
        <img src={img}/>
    </div>
  </div>
);

export default Browser;
