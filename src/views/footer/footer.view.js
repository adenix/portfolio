import React, { Component } from 'react';

import './footer.view.sass';

class Footer extends Component {
  render() {
    return (
      <footer>
        <div>
          <a href="https://github.com/adenix/" target="_blank"><i className="fab fa-github fa-2x"/></a>
          <a href="https://bitbucket.org/adenix/" target="_blank"><i className="fab fa-bitbucket fa-2x"/></a>
          <a href="https://www.linkedin.com/in/adenix/" target="_blank"><i className="fab fa-linkedin fa-2x"/></a>
          <a href="https://twitter.com/adenixx/" target="_blank"><i className="fab fa-twitter fa-2x"/></a>
        </div>
      </footer>
    )
  }
}

export default Footer;
