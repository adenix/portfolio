import React, {Component} from 'react';

import './skills.view.sass';
import skills from './skills.json';
import Grid from "../../../components/grid";

class Skills extends Component {
  render() {
    return (
      <section id="skills">
        <Grid>
          {
            skills.map((skill, i) => {
              return (
                <a key={i} href={skill.href} target="window">
                  {
                    this.renderImage(skill)
                  }
                </a>
              )
            })
          }
        </Grid>
      </section>
    )
  }

  renderImage = (skill) => {
    if (skill.type === 'fa')
      return (
        <i className={skill.pack + ' fa-' + skill.icon}/>
      );
    else if (skill.type === 'svg-em')
      return (
        <span dangerouslySetInnerHTML={{__html: skill.svg}}/>
      );
  }
}

export default Skills;
