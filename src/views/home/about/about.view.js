import React, {Component} from 'react';

import './about.view.sass';
import commands from "./console.json";

import Console from "../../../components/console";
import Command from "../../../components/console/command";

class About extends Component {
  render() {
    return (
      <section id="about">
        <Console shadow>
          {
            commands.map((command, i) => {
              return (
                <Command statement={command.body} responses={command.responses} key={i}/>
              )
            })
          }
        </Console>
      </section>
    )
  }
}

export default About;
