import React, {Component} from 'react';

import './projects.view.sass';
import projects from './projects.json';

import Browser from "../../../components/browser";
import Tag from "../../../components/tag";

function getSupportedImageType() {
  if (Modernizr.webp) return "webp";
  if (Modernizr.jpeg2000) return "jp2";
  return "png";
}

class Projects extends Component {
  render() {
    const imageKey = getSupportedImageType();
    return (
      <section id="projects">
        <div className="title">Projects</div>
        {
          projects.map((project, i) => (
            <div className="project"
                 key={i}>
              <Browser protocol={project.url.protocol}
                       baseUrl={project.url.base}
                       path={project.url.path}
                       img={project.image[imageKey]}/>
              <div className="project-body">
                <h2>{project.title}</h2>
                {
                  project.body.map((p, j) => (
                    <p key={j}>{p}</p>
                  ))
                }
                {
                  project.tags.map((tag, j) => (
                    <Tag key={j}>{tag}</Tag>
                  ))
                }
              </div>
            </div>
          ))
        }
      </section>
    )
  }
}

export default Projects;
