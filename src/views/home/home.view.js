import React, { Component } from 'react';

import './home.view.sass';

import Header from "../header";
import About from './about';
import Projects from "./projects";
import Experience from "./experience";
import Skills from "./skills";
import Foot from "../footer";

class Home extends Component {
  render() {
    return (
      <div>
        <Header/>
        <About/>
        <Projects/>
        <Experience/>
        <Skills/>
        <Foot/>
      </div>
    )
  }
}

export default Home;
