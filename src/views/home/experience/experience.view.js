import React, {Component} from 'react';

import './exerience.view.sass';
import projects from './experience.json';

import Timeline from "../../../components/timeline";
import TimelineItem from "../../../components/timeline/timeline-item";

class Experience extends Component {
  render() {
    return (
      <section id="experience">
        <div className="title">Experience</div>
        <Timeline>
          {
            projects.map((project, i) => {
              return (
                <TimelineItem key={i}
                              date={project.date}
                              sub={project.sub}
                              title={project.title}>
                  <ul>
                    {
                      project.body.map((li, i) => {
                        return (
                          <li key={i}>{li}</li>
                        )
                      })
                    }
                  </ul>
                </TimelineItem>
              );
            })
          }
        </Timeline>
      </section>
    )
  }
}

export default Experience;
