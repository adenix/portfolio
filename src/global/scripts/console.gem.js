import pjson from "../../../package.json";

const cssWeightBold = "font-weight: bold;";
const cssWeightRegular = "font-weight: regular;";
const cssStyleItallic = "font-style: italic;";
const cssStyleRegular = "font-style: regular;";

const ConsoleGems = () => {

  window.version = pjson.version;

  console.info(`%cHello there! Welcome to the console!

%cAs my page says.. my name is Austin and my username is adenix. Why adenix?.. Great question!

%cA   %c -  %cA%custin
%cDE  %c -  %cDE%can
%cNIX %c -  %cNI%c(%cX%c)cholas

..okay, so Ill admit that the x kind of breaks the pattern. But %cadenix%c sounds a lot cooler than %cadenic%c!`,
cssWeightBold, cssWeightRegular, cssWeightBold, cssWeightRegular, cssWeightBold, cssWeightRegular, cssWeightBold,
cssWeightRegular, cssWeightBold, cssWeightRegular, cssWeightBold, cssWeightRegular, cssWeightBold, cssWeightRegular,
cssWeightBold, cssWeightRegular, cssStyleItallic, cssStyleRegular, cssStyleItallic, cssStyleRegular);
};

export default ConsoleGems;
